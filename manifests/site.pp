node default {
	common::swap { 'swap':
		ensure => present,
		swapsize => 2048,
	}
	class { 'fsvan::jenkins_master' :
	} ~>
	class { 'fsvan::jenkins_android_agent':
	}
	firewall { '000 accept http on 80,8080,8181':
		port => [80,8080,8181],
		proto => tcp,
		action => accept,
	}
	firewall { '001 accept on 1521':
		port => [1521],
		proto => tcp,
		action => accept,
	}
	class { 'fsvan::jboss_server': }
}
class fsvan::jboss_server {
	class { 'jboss':
		disable => '',
	}
	
	jboss::instance { 'dev01':
		createuser => false,
		bindaddr => '0.0.0.0',
		port => '8181',
	}
}
class fsvan::dev_tools {
	package { 'git': }
}
class fsvan::jvm_tools {
	include maven	
	class { 'gradle':
		version => '1.8',
	}
	class { 'java':
		distribution => 'jdk',
		version => 'latest',
	}
}
class fsvan::jenkins_master {
	include jenkins
	class {'fsvan::dev_tools':}
	class {'fsvan::jvm_tools':}

	class { 'apache': 
	}
	
	file { '/etc/motd':
		content => 'Welcome to your Jenkins master',
	}
}

class fsvan::jenkins_android_agent {
	class { 'android':
		user => 'jenkins',
		group => 'jenkins',
	}
	#android::platform { 'android-8': }
	#android::platform { 'android-10': }
	#android::platform { 'android-12': }
	#android::platform { 'android-13': }
	#android::platform { 'android-14': }
	#android::platform { 'android-15': }
	android::platform { 'android-16': }
	#android::platform { 'android-17': }
	#android::platform { 'android-18': }

	android::addon {'extra-android-support': }
	android::addon {'extra-google-play': }
}

# https://gist.github.com/philfreo/6576492
define common::swap (
  $ensure       = 'present',
  $swapfile     = "/mnt/${title}",
  $swapsize     = $::memorysize_mb
  ) {
  $swapsizes = split("${swapsize}",'[.]')
  $swapfilesize = $swapsizes[0]
 
  file_line { "swap_fstab_line_${swapfile}":
    ensure  => $ensure,
    line    => "${swapfile} swap swap defaults 0 0",
    path    => "/etc/fstab",
    match   => "${swapfile}",
  }
 
  if $ensure == 'present' {
    exec { 'Create swap file':
      command => "/bin/dd if=/dev/zero of=${swapfile} bs=1M count=${swapfilesize}",
      creates => $swapfile,
    }
 
    exec { 'Attach swap file':
      command => "/sbin/mkswap ${swapfile} && /sbin/swapon ${swapfile}",
      require => Exec['Create swap file'],
      unless  => "/sbin/swapon -s | grep ${swapfile}",
    }
 
  } elsif $ensure == 'absent' {
    exec { 'Detach swap file':
      command => "/sbin/swapoff ${swapfile}",
      onlyif  => "/sbin/swapon -s | grep ${swapfile}",
    }
    file { $swapfile:
      ensure  => absent,
      require => Exec['Detach swap file'],
      backup => false
    }
    
  }
}
